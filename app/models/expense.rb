class Expense < ApplicationRecord
  belongs_to :category

  validates :title, presence: true
  validates :amount, presence: true
  validates :amount, numericality: { greater_than: 0 }
end
