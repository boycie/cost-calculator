class ApplicationController < ActionController::Base

  before_action :log_session_id
  before_action :set_p3p

  def set_p3p
    response.headers["P3P"]='CP="CAO PSA OUR"'
  end

  def log_session_id
    logger.info '  Session ID: ' + (request.session_options[:id] || 'N/A')
  end
  
end
