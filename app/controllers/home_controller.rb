class HomeController < ApplicationController
  def index
    @expenses = Expense.where("created_at > ?", 7.days.ago)
  end
end
