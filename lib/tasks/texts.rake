require 'csv'

namespace :texts do

  desc "Export current locale translations to csv format"
  task export: :environment do
    yaml = YAML.load_file(Rails.root.join("config/locales/#{ I18n.locale }.yml"))
    CSV.open(Rails.root.join("config/locales/#{ I18n.locale }.csv"), "wb") do |csv|
      extract(csv, yaml)
    end
    puts "Translations exported to #{ Rails.root.join("config/locales/#{ I18n.locale }.csv") }"
  end

  def get_hash(csv, hash, prefix = [])
    hash.each do |key, value|
      extract(csv, value, prefix + [key])
    end
  end

  def get_array(csv, hash, prefix = [])
    hash.each_with_index do |object, index|
      extract(csv, object, prefix + ["[#{ index }]"])
    end
  end

  def extract(csv, object, prefix = [])
    if object.kind_of?(Hash)
      get_hash(csv, object, prefix)
    elsif object.kind_of?(Array)
      get_array(csv, object, prefix)
    else
      csv << [prefix.join('.'), object.to_s]
    end
  end
end