Given /^there (?:is|are) (\d+)( \w+)? (\w+)(?:| with {(.*)})$/ do |count, trait, model, params|
  p = params ? Hash[params.split(/(?<!\\),/).map{ |x| x.gsub('\,', ',').split(': ').map(&:strip) }] : {}
  count.to_i.times do
    if trait.present?
      FactoryBot.create(model.singularize, trait.strip.to_sym, p)
    else
      FactoryBot.create(model.singularize, p)
    end
  end
end