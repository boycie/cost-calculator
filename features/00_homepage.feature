Feature: Visit homepage
  Scenario: User should be able to visit homepage
    When I am on the home page
    Then I should see "Aplikacija za praćenje mjesečnih troškova"

  Scenario: User should see notification if there is no new expenses in the last week
    When I am on the home page
    Then I should see "U zadnjih tjedan dana nije bilo troškova"