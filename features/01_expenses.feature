Feature: Visit Expenses page
  Scenario: User should be able to visit Expenses page
    Given I am on the home page
    When I click on "Troškovi" within "header"
    Then I should be on the expenses page
    And I should see 'Lista troškova'

  Scenario: User should be able to create new expense
    Given I am on the home page
    When I click on "Kreiraj novi zapis"
    Then I should be on the new_expense page
    And I should see "Kreiraj novi zapis"