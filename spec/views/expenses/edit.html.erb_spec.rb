require 'rails_helper'

RSpec.describe "expenses/edit", type: :view do
  before(:each) do
    @expense = assign(:expense, Expense.create!(
      :title => "MyString",
      :amount => 1,
      :category => nil,
      :description => "MyText"
    ))
  end

  it "renders the edit expense form" do
    render

    assert_select "form[action=?][method=?]", expense_path(@expense), "post" do

      assert_select "input[name=?]", "expense[title]"

      assert_select "input[name=?]", "expense[amount]"

      assert_select "input[name=?]", "expense[category_id]"

      assert_select "textarea[name=?]", "expense[description]"
    end
  end
end
