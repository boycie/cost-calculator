FactoryBot.define do
  factory :expense do
    title "MyString"
    amount 1
    category nil
    description "MyText"
  end
end
