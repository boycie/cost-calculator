class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.string :title
      t.integer :amount
      t.references :category, foreign_key: true
      t.text :description

      t.timestamps
    end
  end
end
