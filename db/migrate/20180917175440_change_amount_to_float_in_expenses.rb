class ChangeAmountToFloatInExpenses < ActiveRecord::Migration[5.2]
  def change
    change_column :expenses, :amount, :float
  end
end
